<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HijosPadres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('padreshijos', function (Blueprint $table) {
          $table->increments('id');
          $table->integer("hijo")->unsigned();;
          $table->integer("padre")->unsigned();;
          $table->string("desc");
          $table->timestamps();
          $table->foreign('hijo')
                ->references('id')->on('personas');
          $table->foreign('padre')
                ->references('id')->on('personas');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
