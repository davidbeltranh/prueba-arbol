<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PersonaTable extends Migration
{

    public function up()
    {
      Schema::create('personas', function (Blueprint $table) {
          $table->increments('id');
          $table->string("nombres");
          $table->string("apellidos");
          $table->timestamps();
      });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
