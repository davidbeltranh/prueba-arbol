<?php

namespace prueba\Http\Controllers;

use Illuminate\Http\Request;

use prueba\Http\Requests;
use prueba\PadresHijos;

class ArbolController extends Controller
{
   public function getIndex(){
       $consulta = PadresHijos::getPersonas();
       $nombre = "Maria Herrera";
       return view('welcome')->with(["consulta"=>$consulta, 'nombre'=>$nombre]);
   }
}
