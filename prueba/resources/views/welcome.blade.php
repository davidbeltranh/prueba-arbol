<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Arbol</title>
        <link rel="stylesheet" href="{{url('/')}}/archivos/css/bootstrap.css">
        <link rel="stylesheet" href="{{url('/')}}/archivos/css/index.css">
    </head>
    <body>
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="navbar-header">
               <a title="Arbol Genealogico" class="navbar-brand" href="#"><i  class = "fa fa-comments-o" ></i> Arbol Genealogico</a>
            </div>
            <div id="navbarCollapse" class="collapse navbar-collapse navbar-ex1-collapse">
            </div>  
            <div style="background-color:#ff420e;width:34%;height:5px;float:left;" ></div>  
            <div style="background-color:#ff950e;width:32%;height:5px;float:left;" ></div>
            <div style="background-color:#579d1c;width:34%;height:5px;float:left;" ></div>
        </div>        
        <div class="space-30"></div>
        <div class="space-10"></div>
        <div class="row no-margin">
            <div class="col-xs-12" id="contenido">
                <div class="alert alert-success text-center">
                    <h4>Arbol de:
                        <small> {{$nombre}} </small>
                    </h4>    
                </div>
            </div>
            
            <div class="space-30"></div>
            
            <div class="col-xs-12 text-center">
                <div class="col-xs-7">
                    <div class="col-xs-6"></div>
                    <div class="col-xs-6">
                        <div class="alert alert-danger">
                            <h5>
                                {{$consulta[0]->nombres}} {{$consulta[0]->apellidos}} 
                                <small>(madre)</small>
                            </h5>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="alert alert-info">
                            <h5>    
                                {{$consulta[2]->nombres}} {{$consulta[2]->apellidos}} 
                                <small>(madre)</small>
                            </h5>    
                        </div>
                    </div> 
                    <div class="col-xs-6">
                        <div class="alert alert-info">
                            <h5> 
                                {{$consulta[1]->nombres}} {{$consulta[1]->apellidos}} 
                                <small>(padre)</small>
                            </h5>     
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="alert alert-warning">
                            <h5> 
                                {{$consulta[5]->nombres}} {{$consulta[5]->apellidos}}
                                <small>(padre)</small>
                            </h5>    
                        </div>
                    </div>
                </div>    
                <div class="col-xs-5">
                    <div class="space-30"></div>
                    <div class="space-13"></div>
                    <div class="col-xs-12">
                        <div class="alert alert-danger">
                            <h5>    
                                {{$consulta[3]->nombres}} {{$consulta[3]->apellidos}}
                                <small>(madre)</small>
                            </h5>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="alert alert-warning">
                            <h5>
                                {{$consulta[6]->nombres}} {{$consulta[6]->apellidos}}
                                <small>(madre)</small>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="col-xs-2"></div>
                <div class="col-xs-8">
                    <div class="col-xs-6">
                        <div class="alert alert-info">
                            {{$consulta[7]->nombres}} {{$consulta[7]->apellidos}}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="alert alert-info">
                            {{$consulta[8]->nombres}} {{$consulta[8]->apellidos}}
                        </div>
                    </div>
                </div>
                <div class="col-xs-2"></div>
            </div>
        </div>
    </body>
    <script src="{{url('/')}}/archivos/js/jquery-2.1.1.js"></script>
    <script src="{{url('/')}}/archivos/js/bootstrap.js"></script>
</html>